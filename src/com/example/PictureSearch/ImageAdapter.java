package com.example.PictureSearch;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CursorAdapter;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.List;

public class ImageAdapter extends CursorAdapter {

	private Bitmap defaultThumbnail;
	private final static ThumbnailCache cache = ThumbnailCache.getInstance();
	public final static int LOAD_SUCCESS = 0;

	private SparseBooleanArray checked;

	static class ViewHolder {
		ImageView imageView;
		CheckBox checkBox;
		String fullSizeUri;
	}

	public ImageAdapter(final Context context, final Cursor c, final boolean autoRequery) {
		super(context, c, autoRequery);
		checked = new SparseBooleanArray();
	}

	@Override
	public View newView(final Context context, final Cursor cursor, final ViewGroup viewGroup) {
		final View view = LayoutInflater.from(context).inflate(R.layout.gallery_item, null);
		final ViewHolder holder = new ViewHolder();
		if (defaultThumbnail == null) {
			defaultThumbnail = BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_launcher);
		}
		holder.imageView = (ImageView)view.findViewById(R.id.thumbnail);
		holder.checkBox = (CheckBox)view.findViewById(R.id.itemCheckBox);
		view.setTag(holder);
		MainActivity.ImageCheckListener.setChecked(checked);
		return view;
	}

	@Override
	public void bindView(final View view, final Context context, final Cursor cursor) {
		final ViewHolder holder = (ViewHolder)view.getTag();
		final MainActivity.ImageCheckListener concreteListener = new MainActivity.ImageCheckListener();
		final AsyncDrawable drawable = new AsyncDrawable(Uri.parse(cursor.getString(1)), defaultThumbnail);
		holder.imageView.setImageDrawable(drawable);
		holder.fullSizeUri = cursor.getString(1);
		final int realPosition = cursor.getInt(0);
		concreteListener.setPosition(realPosition);
		if (MainActivity.ImageCheckListener.getChecked() == null) {
			MainActivity.ImageCheckListener.setChecked(checked);
		}
		holder.checkBox.setOnCheckedChangeListener(concreteListener);
		holder.checkBox.setChecked(checked.get(realPosition));
	}

	public boolean hasCheckedItems() {
		return checked.indexOfValue(true) >= 0;
	}

	public void clearSelection() {
		for (int i = 0; i < checked.size(); i++) {
			checked.put(i, false);
		}
	}

	public List<Integer> getAllSelectedItems() {
		final List<Integer> res = new ArrayList<Integer>();
		for (int i = 0; i < checked.size(); i++) {
			if (checked.get(i)) {
				res.add(i);
			}
		}
		return res;
	}

	public void removeItems(final List<Integer> toDelete) {
		for (Integer item : toDelete) {
			checked.delete(item);
		}
	}

	static class AsyncDrawable extends Drawable {

		private final Bitmap defaultBitmap;
		private final Uri uri;
		private Handler handler;
		private static Paint paint= new Paint();

		AsyncDrawable(final Uri uri, final Bitmap defaultImage) {
			this.defaultBitmap = defaultImage;
			this.uri = uri;
			handler = new Handler(new Handler.Callback() {

				@Override
				public boolean handleMessage(final Message message) {
					if (message.what == LOAD_SUCCESS) {
						invalidateSelf();
					}
					return true;
				}
			});

		}


		@Override
		public void draw(final Canvas canvas) {
			Bitmap actualBitmap = cache.getThumbnail(uri, new BitmapCreatedListener() {
				@Override
				public void onSuccess(final Bitmap bitmap) {
					if (handler != null) {
						handler.sendEmptyMessage(LOAD_SUCCESS);
					}
				}

				@Override
				public void onError() {
					Log.d("", "Cannot create thumbnail");
				}
			});
			if (actualBitmap == null) {
				canvas.drawBitmap(defaultBitmap, 0, 0, paint);
			}  else {
				canvas.drawBitmap(actualBitmap, 0, 0, paint);
			}

		}

		@Override
		public void setAlpha(final int i) {

		}

		@Override
		public void setColorFilter(final ColorFilter colorFilter) {

		}

		@Override
		public int getOpacity() {
			return 0;
		}
	}

}
