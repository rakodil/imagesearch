package com.example.PictureSearch;

import android.app.SearchManager;
import android.content.Intent;
import android.content.SearchRecentSuggestionsProvider;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.net.Uri;

public class RequestSuggestionProvider extends SearchRecentSuggestionsProvider {
	public static final String AUTHORITY = "com.example.PictureSearch.RequestSuggestionProvider";

	public static final int MODE = DATABASE_MODE_QUERIES;

	public RequestSuggestionProvider() {
		setupSuggestions(AUTHORITY, MODE);
	}

	@Override
	public Cursor query(
			final Uri uri,
			final String[] projection,
			final String selection,
			final String[] selectionArgs,
			final String sortOrder
	) {
		if ("".equals(selectionArgs[0])) {
			final Cursor suggestCursor = getContext().getContentResolver().query(
					ImagesProvider.GET_REQUESTS_URI,
					new String[] {
							ImagesProvider.REQUEST_ID,
							ImagesProvider.REQUEST_QUERY_WITH_TABLE + " as " + SearchManager.SUGGEST_COLUMN_TEXT_1
					},
					null,
					null,
					null
			);
			final String[] columnNames = new String[] {
					"_id",
				    SearchManager.SUGGEST_COLUMN_TEXT_1,
					SearchManager.SUGGEST_COLUMN_INTENT_ACTION,
					SearchManager.SUGGEST_COLUMN_INTENT_EXTRA_DATA
			};
			final MatrixCursor result = new MatrixCursor(columnNames, suggestCursor.getCount());
			while (suggestCursor.moveToNext()) {
				result.addRow(new Object[] {
						suggestCursor.getInt(0),
						suggestCursor.getString(1),
						Intent.ACTION_VIEW,
						suggestCursor.getString(1)
				});
			}
			suggestCursor.close();
			return result;
		}
		return super.query(uri, projection, selection, selectionArgs, sortOrder);
	}
}
