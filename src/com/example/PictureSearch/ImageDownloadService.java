package com.example.PictureSearch;

import android.app.IntentService;
import android.app.SearchManager;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;
import org.apache.http.conn.util.InetAddressUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.NetworkInterface;
import java.net.URL;
import java.net.URLConnection;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ImageDownloadService extends IntentService {

	public static final String URL_JSON = "url";
	public static final int MAX_RESULT_PAGES = 8;
	public static final int MAX_RESULTS_IN_ONE_PAGE = 8;
	public static final String RESPONSE_DATA_JSON = "responseData";
	public static final String RESULTS_JSON = "results";
	public static final String BASE_URL = "https://ajax.googleapis.com/ajax/services/search/images?v=1.0&rsz=8&as_filetype=jpg&userip=";

	public ImageDownloadService() {
		super("Image Download Service");
	}

	@Override
	public int onStartCommand(final Intent intent, final int flags, final int startId) {
		return super.onStartCommand(intent, flags, startId);
	}

	@Override
	public IBinder onBind(final Intent intent) {
		return null;
	}

	@Override
	protected void onHandleIntent(final Intent intent) {
		final String request = intent.getStringExtra(SearchManager.QUERY);
		if (!isNetworkAvailable()) {
			Toast.makeText(this, getString(R.string.no_internet_warning), Toast.LENGTH_SHORT).show();
		}
		if (!isAlreadySearched(request)) {
			searchAndSave(request);
		}
	}

	private boolean isAlreadySearched(final String query) {
		final Cursor cursor = getContentResolver().query(
				ImagesProvider.REQUEST_URI,
				new String[]{"count (*)"},
				String.format("%s = ?", ImagesProvider.REQUEST_QUERY_WITH_TABLE),
				new String[]{query},
				null);
		if (cursor.moveToFirst()) {
			return cursor.getInt(0) > 0;
		}
		return false;
	}

	private void searchAndSave(final String query) {
		final List<String> urls = new ArrayList<String>(MAX_RESULT_PAGES * MAX_RESULTS_IN_ONE_PAGE);
		for (int i = 0; i < MAX_RESULT_PAGES; i++) {
			int nextPageStart = i * MAX_RESULTS_IN_ONE_PAGE;
			String url = makeURLFromQuery(query, nextPageStart);
			performSearch(url, urls);
		}
		final ContentValues requestValues = new ContentValues();
		requestValues.put(ImagesProvider.REQUEST_QUERY, query);
		Uri res = getContentResolver().insert(ImagesProvider.INSERT_REQUEST_URI, requestValues);
		final ContentValues imageValues = new ContentValues();
		imageValues.put(ImagesProvider.REQUEST, res.getLastPathSegment());
		for (final String u : urls) {
			try {
				final String filename = downloadAndReturnName(u);
				imageValues.remove(ImagesProvider.FILENAME);
				imageValues.put(ImagesProvider.FILENAME, getFilesDir() + "/" + filename);
				getContentResolver().insert(ImagesProvider.INSERT_IMAGE_URI, imageValues);
				getContentResolver().notifyChange(ImagesProvider.REQUEST_URI, null);
			} catch (Exception e) {
				Log.d("", "Cannot download or save file");
			}
		}
	}

	private String downloadAndReturnName(final String url) throws IOException {
		final String filename = toMD5(url);
		URLConnection connection = new URL(url).openConnection();
		final Cursor isPresent = getContentResolver().query(
				ImagesProvider.GET_IMAGES_BY_NAME,
				new String[]{"count (*)"},
				String.format("%s = ?", ImagesProvider.IMAGE_FILENAME_WITH_TABLE),
				new String[]{filename},
				null
		);
		isPresent.moveToNext();
		if (isPresent.getInt(0) > 0) {
			isPresent.close();
			return filename;
		}
		isPresent.close();
		InputStream inputStream = connection.getInputStream();
		Bitmap image = BitmapFactory.decodeStream(inputStream);
		FileOutputStream fos = openFileOutput(filename, Context.MODE_PRIVATE);
		image.compress(Bitmap.CompressFormat.JPEG, 80, fos);
		fos.close();
		return filename;
	}

	public static String toMD5(String s) {
		try {
			MessageDigest digest = java.security.MessageDigest.getInstance("MD5");
			digest.update(s.getBytes());
			byte messageDigest[] = digest.digest();

			StringBuilder hexString = new StringBuilder();
			for (byte b : messageDigest) {
				String h = Integer.toHexString(0xFF & b);
				while (h.length() < 2) {
					h = "0" + h;
				}
				hexString.append(h);
			}
			return hexString.toString();

		} catch (NoSuchAlgorithmException e) {
			Log.e("", "MD5 algorithm is not found.", e);
		}
		return null;
	}

	private void performSearch(final String query, final List<String> resultUrls) {
		try {
			URL url = new URL(query);
			URLConnection connection = url.openConnection();
			String line;
			StringBuilder builder = new StringBuilder();
			BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
			while ((line = reader.readLine()) != null) {
				builder.append(line);
			}
			JSONObject json = new JSONObject(builder.toString());
			JSONArray results = json.getJSONObject(RESPONSE_DATA_JSON).getJSONArray(RESULTS_JSON);
			for (int i = 0; i < results.length(); i++) {
				resultUrls.add(results.getJSONObject(i).getString(URL_JSON));
			}
		} catch (MalformedURLException e) {
			Log.d("", "Url is malformed.");
		} catch (IOException e) {
			Log.d("", "Cannot open connection");
		} catch (JSONException e) {
			Log.d("", "Invalid JSON");
		}
		Toast.makeText(this, "Something went wrong", Toast.LENGTH_SHORT);
	}

	private String makeURLFromQuery(final String query, final int from) {
		StringBuffer res = new StringBuffer(BASE_URL);
		res.append(getIPAddress()).append("&q=").append(query.replaceAll("\\s", "%20")).append("&start=").append(from);
		return res.toString();
	}

	public static String getIPAddress() {
		try {
			List<NetworkInterface> interfaces = Collections.list(NetworkInterface.getNetworkInterfaces());
			for (NetworkInterface intf : interfaces) {
				List<InetAddress> addrs = Collections.list(intf.getInetAddresses());
				for (InetAddress addr : addrs) {
					if (!addr.isLoopbackAddress()) {
						String sAddr = addr.getHostAddress().toUpperCase();
						if (InetAddressUtils.isIPv4Address(sAddr))
							return sAddr;
					}
				}
			}
		} catch (Exception ex) {
			Log.d("", "Cannot get IP");
		}
		return "";
	}

	private boolean isNetworkAvailable() {
		ConnectivityManager connectivityManager
				= (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
		return activeNetworkInfo != null && activeNetworkInfo.isConnected();
	}
}
