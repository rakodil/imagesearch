package com.example.PictureSearch;

import android.app.ListActivity;
import android.app.LoaderManager;
import android.app.SearchManager;
import android.content.Context;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.SearchRecentSuggestions;
import android.text.TextUtils;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.ActionMode;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SearchView;

import java.util.List;

public class MainActivity extends ListActivity implements LoaderManager.LoaderCallbacks<Cursor> {

	private static final int SEARCH_URL_LOADER = 1;
	private String searchQuery = "";
	private ActionModeHelper helper = new ActionModeHelper();
	private ActionMode actionMode;
	private boolean fullSizeMode;

	private ImageAdapter adapter = new ImageAdapter(this, null, false);
	private ImageView fullSizeImage;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.gallery);
		setListAdapter(adapter);
		registerForContextMenu(getListView());
		getLoaderManager().initLoader(SEARCH_URL_LOADER, null, this);
		getListView().setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);
		ImageCheckListener.setView(getListView());
		getListView().setMultiChoiceModeListener(helper);
		getListView().setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(final AdapterView<?> adapterView, final View view, final int i, final long l) {
				fullSizeMode = true;
				ImageAdapter.ViewHolder holder = (ImageAdapter.ViewHolder) view.getTag();
				getListView().setVisibility(View.GONE);
				fullSizeImage = (ImageView) findViewById(R.id.fullSizeImage);
				fullSizeImage.setImageURI(Uri.parse(holder.fullSizeUri));
				fullSizeImage.setVisibility(View.VISIBLE);
			}
		});
	}

	@Override
	public void onBackPressed() {
		if (fullSizeMode) {
			getListView().setVisibility(View.VISIBLE);
			fullSizeMode = false;
			fullSizeImage.setVisibility(View.GONE);
			return;
		}
		super.onBackPressed();
	}

	@Override
	public void onCreateContextMenu(final ContextMenu menu, final View v, final ContextMenu.ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, v, menuInfo);
	}

	@Override
	public boolean onCreateOptionsMenu(final Menu menu) {
		getMenuInflater().inflate(R.menu.main_activity_menu, menu);
		final SearchManager manager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
		final SearchView searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
		searchView.setSearchableInfo(manager.getSearchableInfo(getComponentName()));
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	protected void onNewIntent(final Intent intent) {
		if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
			Intent newIntent = new Intent(this, ImageDownloadService.class);
			searchQuery = intent.getStringExtra(SearchManager.QUERY);
			newIntent.putExtra(SearchManager.QUERY, searchQuery);
			startService(newIntent);
			saveSuggestions(searchQuery);
		} else if (Intent.ACTION_VIEW.equals(intent.getAction())) {
			searchQuery = intent.getStringExtra(SearchManager.EXTRA_DATA_KEY);
		}
		setIntent(intent);
		getLoaderManager().restartLoader(SEARCH_URL_LOADER, null, this);
	}

	@Override
	public Loader<Cursor> onCreateLoader(final int i, final Bundle bundle) {
		switch (i) {
			case SEARCH_URL_LOADER:
				return new CursorLoader(
						this,
						ImagesProvider.REQUEST_URI,
						new String[]{ImagesProvider.IMAGE_ID_WITH_TABLE, ImagesProvider.IMAGE_FILENAME_WITH_TABLE, ImagesProvider.IMAGE_REQUEST_WITH_TABLE},
						String.format("%s = ?", ImagesProvider.REQUEST_QUERY_WITH_TABLE),
						new String[]{searchQuery},
						null
				);
			default:
				return null;
		}
	}

	@Override
	public void onLoadFinished(final Loader<Cursor> cursorLoader, final Cursor cursor) {
		adapter.changeCursor(cursor);
	}

	@Override
	public void onLoaderReset(final Loader<Cursor> cursorLoader) {
		adapter.changeCursor(null);
	}


	private void saveSuggestions(final String suggestion) {
		final SearchRecentSuggestions suggestions = new SearchRecentSuggestions(
				this,
				RequestSuggestionProvider.AUTHORITY,
				RequestSuggestionProvider.MODE
		);
		suggestions.saveRecentQuery(suggestion, null);
	}

	static class ImageCheckListener implements CompoundButton.OnCheckedChangeListener {
		private int position;
		private static ListView view;
		private static SparseBooleanArray checked;

		static void setView(final ListView view) {
			ImageCheckListener.view = view;
		}

		ImageCheckListener() {

		}

		@Override
		public void onCheckedChanged(final CompoundButton compoundButton, final boolean b) {
			view.setItemChecked(position, b);
			checked.put(position, b);
			Log.d("", String.format("Putting %d position as %b", position, b));
			Log.d("", String.format("Setting item check at position %d to %b", position, b));
		}

		static SparseBooleanArray getChecked() {
			return checked;
		}

		static void setChecked(final SparseBooleanArray checked) {
			ImageCheckListener.checked = checked;
		}

		void setPosition(final int position) {
			this.position = position;
		}
	}

	class ActionModeHelper implements AbsListView.MultiChoiceModeListener {


		@Override
		public void onItemCheckedStateChanged(final ActionMode mode, final int position, final long id, final boolean checked) {
			if (actionMode == null && checked) {
				actionMode = startActionMode(this);
			} else {
				if ((actionMode != null && checked) || (adapter.hasCheckedItems())) {
					return;
				} else if (!adapter.hasCheckedItems()) {
					actionMode.finish();
				}
			}
		}

		@Override
		public boolean onCreateActionMode(final ActionMode mode, final Menu menu) {
			getMenuInflater().inflate(R.menu.main_activity_context_menu, menu);
			return true;
		}

		@Override
		public boolean onPrepareActionMode(final ActionMode mode, final Menu menu) {
			return false;
		}

		@Override
		public boolean onActionItemClicked(final ActionMode mode, final MenuItem item) {
			switch (item.getItemId()) {
				case R.id.actionDelete:
					List<Integer> toDel = adapter.getAllSelectedItems();
					adapter.removeItems(toDel);
					getContentResolver().delete(ImagesProvider.DELETE_IMAGES_BY_ID,
							String.format("%s in ?", ImagesProvider.IMAGE_ID_WITH_TABLE),
							new String[]{TextUtils.join(", ", toDel)});
					getContentResolver().notifyChange(ImagesProvider.REQUEST_URI, null);
					return true;
				default:
					return false;
			}
		}

		@Override
		public void onDestroyActionMode(final ActionMode mode) {
			adapter.clearSelection();
		}
	}

}
