package com.example.PictureSearch;

import android.graphics.Bitmap;

public interface BitmapCreatedListener {
	void onSuccess(final Bitmap bitmap);
	void onError();
}
