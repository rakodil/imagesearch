package com.example.PictureSearch;

import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;

public class ImagesProvider extends ContentProvider {

	private static final String DB_NAME = "images";
	public static final String REQUEST_TABLE = "requests";
	public static final String IMAGES_TABLE = "images";

	private static final String CREATE_TABLE = "create table ";

	private static final String CREATE_REQUESTS = CREATE_TABLE + REQUEST_TABLE +
			" (_id integer primary key asc, request_query text)";
	private static final String CREATE_IMAGES = CREATE_TABLE + IMAGES_TABLE +
			" (_id integer primary key asc, request integer, filename text)";

	public static final String AUTHORITY = "com.example.PictureSearch";

	private static final int INSERT_REQUEST_ID = 1;
	private static final int INSERT_IMAGE_ID = 2;
	private static final int GET_IMAGES_ID = 3;
	private static final int DELETE_REQUEST_ID = 4;
	private static final int GET_IMAGE_BY_NAME_ID = 5;
	private static final int GET_REQUESTS_ID = 7;

	private static final int DELETE_IMAGE_ID = 6;
	public static final String INSERT = "insert";
	public static final String CONTENT = "content";
	public static final String DELETE = "delete";
	public static final String ID = "_id";
	public static final String REQUEST_QUERY = "request_query";
	public static final String SUGGESTIONS = "suggestions";
	public static final String FILENAME = "filename";
	public static final String REQUEST = "request";
	public static final String REQUEST_ID = REQUEST_TABLE + "." + ID;
	public static final String REQUEST_QUERY_WITH_TABLE = REQUEST_TABLE + "." + REQUEST_QUERY;
	public static final String IMAGE_FILENAME_WITH_TABLE = IMAGES_TABLE + "." + FILENAME;
	public static final String IMAGE_REQUEST_WITH_TABLE = IMAGES_TABLE + "." + REQUEST;
	public static final String IMAGE_ID_WITH_TABLE = IMAGES_TABLE + "." + ID;

	public static final Uri REQUEST_URI = new Uri.Builder().scheme(ContentResolver.SCHEME_CONTENT)
			.authority(AUTHORITY).appendPath(CONTENT).appendPath(REQUEST_TABLE).build();
	public static final Uri INSERT_REQUEST_URI = new Uri.Builder().scheme(ContentResolver.SCHEME_CONTENT)
			.authority(AUTHORITY).appendPath(INSERT).appendPath(REQUEST_TABLE).build();
	public static final Uri INSERT_IMAGE_URI = new Uri.Builder().scheme(ContentResolver.SCHEME_CONTENT)
			.authority(AUTHORITY).appendPath(INSERT).appendPath(IMAGES_TABLE).build();
	public static final Uri GET_REQUESTS_URI = new Uri.Builder().scheme(ContentResolver.SCHEME_CONTENT)
			.authority(AUTHORITY).appendPath(CONTENT).appendPath(REQUEST_TABLE).appendPath(SUGGESTIONS).build();
	public static final Uri GET_IMAGES_BY_NAME = new Uri.Builder().scheme(ContentResolver.SCHEME_CONTENT)
			.authority(AUTHORITY).appendPath(CONTENT).appendPath(IMAGES_TABLE).build();
	public static final Uri DELETE_IMAGES_BY_ID = new Uri.Builder().scheme(ContentResolver.SCHEME_CONTENT)
			.authority(AUTHORITY).appendPath(DELETE).appendPath(IMAGES_TABLE).build();

	private static UriMatcher matcher;
	private MyHelper helper;

	static {
		matcher = new UriMatcher(UriMatcher.NO_MATCH);
		matcher.addURI(AUTHORITY, INSERT + "/" + REQUEST_TABLE, INSERT_REQUEST_ID);
		matcher.addURI(AUTHORITY, INSERT + "/" + IMAGES_TABLE, INSERT_IMAGE_ID);
		matcher.addURI(AUTHORITY, CONTENT + "/" + REQUEST_TABLE, GET_IMAGES_ID);
		matcher.addURI(AUTHORITY, DELETE + "/" + REQUEST_TABLE, DELETE_REQUEST_ID);
		matcher.addURI(AUTHORITY, CONTENT + "/" + IMAGES_TABLE, GET_IMAGE_BY_NAME_ID);
		matcher.addURI(AUTHORITY, DELETE + "/" +IMAGES_TABLE, DELETE_IMAGE_ID);
		matcher.addURI(AUTHORITY, CONTENT + "/" + REQUEST_TABLE + "/" + SUGGESTIONS, GET_REQUESTS_ID);
	}

	@Override
	public boolean onCreate() {
		helper = new MyHelper(this.getContext(), DB_NAME, null, 1);
		return true;
	}

	@Override
	public Cursor query(
			final Uri uri,
			final String[] projection,
			final String selection,
			final String[] selectionArgs,
			final String sortOrder
	) {
		final SQLiteDatabase db = helper.getReadableDatabase();
		SQLiteQueryBuilder builder = new SQLiteQueryBuilder();
		switch (matcher.match(uri)) {
			case GET_IMAGES_ID:
				builder.setTables(String.format("%s, %s", REQUEST_TABLE, IMAGES_TABLE));
				builder.appendWhere(String.format("%s = %s", REQUEST_ID, IMAGE_REQUEST_WITH_TABLE));
				break;
			case GET_IMAGE_BY_NAME_ID:
				builder.setTables(IMAGES_TABLE);
				break;
			case GET_REQUESTS_ID:
				builder.setTables(REQUEST_TABLE);
				break;
			default:
				throw new IllegalArgumentException("no such uri");
		}
		final Cursor result = builder.query(db, projection, selection, selectionArgs, null, null, sortOrder);
		result.setNotificationUri(getContext().getContentResolver(), uri);
		return result;
	}

	@Override
	public String getType(final Uri uri) {
		return null;
	}

	@Override
	public Uri insert(final Uri uri, final ContentValues contentValues) {
		final SQLiteDatabase db = helper.getWritableDatabase();
		long result;
		switch (matcher.match(uri)) {
			case INSERT_REQUEST_ID:
				result = db.insert(REQUEST_TABLE, null, contentValues);
				break;
			case INSERT_IMAGE_ID:
				result = db.insert(IMAGES_TABLE, null, contentValues);
				break;
			default:
				throw new IllegalArgumentException("no such uri");
		}
		return ContentUris.withAppendedId(REQUEST_URI, result);
	}

	@Override
	public int delete(final Uri uri, final String selection, final String[] selectionArgs) {
		int res;
		final SQLiteDatabase db = helper.getWritableDatabase();
		switch (matcher.match(uri)) {
			case DELETE_REQUEST_ID:
				db.delete(IMAGES_TABLE, String.format("%s = ", IMAGE_REQUEST_WITH_TABLE), selectionArgs);
				res = db.delete(REQUEST_TABLE, selection, selectionArgs);
				break;
			default:
				throw new IllegalArgumentException("no such uri");
		}
		return res;
	}

	@Override
	public int update(
			final Uri uri,
			final ContentValues contentValues,
			final String selection,
			final String[] selectionArgs
	) {
		throw new UnsupportedOperationException("update is not required for this provider");
	}

	private class MyHelper extends SQLiteOpenHelper {

		public MyHelper(final Context context, final String name, final SQLiteDatabase.CursorFactory factory, final int version) {
			super(context, name, factory, version);
		}

		@Override
		public void onCreate(final SQLiteDatabase sqLiteDatabase) {
			sqLiteDatabase.execSQL(CREATE_REQUESTS);
			sqLiteDatabase.execSQL(CREATE_IMAGES);
		}

		@Override
		public void onUpgrade(final SQLiteDatabase sqLiteDatabase, final int i, final int i2) {

		}
	}
}
