package com.example.PictureSearch;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class ThumbnailCache {
	private static final int DEFAULT_SIZE = 50;
	private static final float LOAD_FACTOR = 0.75f;
	public static final int DEFAULT_CLEAR_SIZE = 10;
	private LinkedHashMap<Uri, Bitmap> thumbnails;
	private Map<Uri, List<BitmapCreatedListener>> queue;
	private Thread worker;
	private Object lock = new Object();
	private static volatile ThumbnailCache instance = new ThumbnailCache();

	private ThumbnailCache() {
		thumbnails = new LinkedHashMap<Uri, Bitmap>(DEFAULT_SIZE, LOAD_FACTOR, true);
		queue = new LinkedHashMap<Uri, List<BitmapCreatedListener>>();
		worker = new Thread(new ThumbnailProducer());
		worker.start();
	}

	public static ThumbnailCache getInstance() {
		ThumbnailCache localInstance = instance;
   		if (localInstance == null) {
			synchronized (ThumbnailCache.class) {
				localInstance = instance;
				if (localInstance == null) {
					instance = localInstance = new ThumbnailCache();
				}
			}
		}
		return localInstance;
	}

	public synchronized Bitmap getThumbnail(final Uri uri, final BitmapCreatedListener listener) {
		if (thumbnails.containsKey(uri)) {
			return thumbnails.get(uri);
		} else {
			putTaskToQueue(uri, listener);
			return null;
		}
	}

	private void putTaskToQueue(final Uri uri, final BitmapCreatedListener listener) {
		List<BitmapCreatedListener> currentListeners = queue.get(uri);
		if (currentListeners == null) {
			currentListeners = new ArrayList<BitmapCreatedListener>();
		}
		currentListeners.add(listener);
		queue.put(uri, currentListeners);
		synchronized (lock) {
			lock.notifyAll();
		}
	}

	private void removeFirstItems() {
		Iterator<LinkedHashMap.Entry<Uri, Bitmap>> entries = thumbnails.entrySet().iterator();
		for (int i = 0; i < DEFAULT_CLEAR_SIZE && entries.hasNext(); i++) {
			entries.next();
			entries.remove();
		}
	}

	private void clearCache() {
		thumbnails.clear();
	}


	private class ThumbnailProducer implements Runnable {
		@Override
		public void run() {
			while (true) {
				if (queue.isEmpty()) {
					synchronized (lock) {
						try {
						 	lock.wait();
						} catch (InterruptedException e) {
							continue;
						}
					}
				} else {
					Iterator<Map.Entry<Uri, List<BitmapCreatedListener>>> iterator = queue.entrySet().iterator();
					Map.Entry<Uri, List<BitmapCreatedListener>> entry = iterator.next();
					iterator.remove();
					Bitmap src;
					try {
						src = BitmapFactory.decodeFile(entry.getKey().getPath());
					} catch (OutOfMemoryError e) {
						clearCache();
						src = BitmapFactory.decodeFile(entry.getKey().getPath());
					}
					if (src != null) {
						Bitmap thumbnail = Bitmap.createScaledBitmap(src, 120, 120, false);
						if (thumbnails.size() >= DEFAULT_SIZE) {
							removeFirstItems();
						}
						thumbnails.put(entry.getKey(), thumbnail);
						for (BitmapCreatedListener listener : entry.getValue()) {
							listener.onSuccess(thumbnail);
						}
					} else {
						for (BitmapCreatedListener listener : entry.getValue()) {
							listener.onError();
						}
					}
				}
			}
		}
	}
}
